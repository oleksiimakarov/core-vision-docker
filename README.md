# core-vision-docker

## docker-webcam

### Prerequisites

Docker

[Docker Community Edition for Windows](https://store.docker.com/editions/community/docker-ce-desktop-windows)


### Build image


`cd docker-webcam`

`docker build -t corevision .`


### Run container


`docker run -p 8000:8000 -it corevision`


### Run corevision http server for webcam:


`cd /home/core-vision-engine`

`python3 http_server.py`


### Open link in browser

`http://localhost:8000`



## docker-develop

### Build image


`cd docker-develop`

`docker build -t corevision_dev .`

### Clone core-vision
corevision engine
`git clone https://gitlab.com/oleksiimakarov/core-vision-engine.git <YOUR_ENGINE_DIR>`

corevision webcam
`git clone https://gitlab.com/aholub-corevalue/core-vision-webcam.git <YOUR_WEBCAM_DIR>`

### Run container

`docker run -p 8000:8000 -v <YOUR_ENGINE_DIR>:/home/core-vision-engine -v <YOUR_WEBCAM_DIR>:/home/core-vision-webcam -it corevision_dev`


### Run corevision http server for webcam:

`cd /home/core-vision-engine`

`python3 http_server.py`

### Prerequisites

Description in progress

#Some useful docker command
```
docker ps
docker stop <CONTAINER ID>
docker images 
docker rmi <IMAGE ID>
docker run -p 8000:8000 -v <REAL_PATH>:<INTERNAL_DOCKER_PATH> -it corevision
docker exec -it <CONTAINER ID> bash
```
